Commerce Taxonomy Reference

Before using this module please go into:

views_handler_filter_term_commerce_product_tid_depth.inc
views_handler_argument_term_commerce_product_tid_depth.inc

and change the term reference field name from "field_structure" to whichever field name you are using for your commerce products.

Hope this is helpful!
yanniboi